**No Video/Camera Animation**

Petite animation de "respiration" d'une icône de caméra barrée, l'opacité varie de manière adoucie pour créer une discrétion bien que présence de l'icône. À utiliser en livestream quand vous coupez votre caméra, ou autres utilisations du même style !
*Codé en HTML et CSS, tout simplement ;)*

**Comment l'utiliser :**
*  Créez une source "Navigateur" d'une taille de 800x600 avec [ce lien](http://tocram2.free.fr/NoCameraAnim/)  (`http://tocram2.free.fr/NoCameraAnim/`).
*  Vérifiez que le CSS personnalisé contienne `body { background-color: rgba(0, 0, 0, 0); }` ou quelque-chose du style, pour être sûr que le fond sera transparent.
*  C'est tout !

Programmé par moi-même tard le soir / la nuit :^)
Si vous voulez l'utiliser, une petite apparition de mon nom ne serait pas de refus, mais bon vous faites ce que vous voulez.
*Icône originale créée par Freepik sur Flaticon.com ([lien de l'image](https://www.flaticon.com/free-icon/no-video_120614))*


**TO DO :**
* [ ]  Faire une animation "d'entrée" en fondu, pour faire apparaître l'icône quand on arrive sur la page, l'animation normale se lancerait après celle-ci. (je ne sais pas trop comment faire ça, mais je trouverais bien un moyen un jour...)
* [ ]  Mettre ce projet sur un hébergeur qui ne va pas faire planter tout mon site s'il n'accepte pas plus de X connexions simultanées ptdr (à vois s'il n'est pas utilisé que par moi)